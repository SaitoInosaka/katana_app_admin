(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-idea-details-idea-details-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/idea-details/idea-details.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/idea-details/idea-details.page.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header *ngIf=\"idea.id\">\n  <ion-toolbar color=\"dark\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/\"></ion-back-button>\n    </ion-buttons>\n    <ion-title style=\"text-align: center;\"\n      >Editando {{ idea.nombre }}...</ion-title\n    >\n  </ion-toolbar>\n</ion-header>\n\n<ion-header *ngIf=\"!idea.id\">\n  <ion-toolbar color=\"dark\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/\"></ion-back-button>\n    </ion-buttons>\n    <ion-title style=\"text-align: center;\">Crear Nuevo</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\">\n  <ion-item>\n    <ion-label position=\"stacked\"><h1>Nombre</h1></ion-label>\n    <ion-input [(ngModel)]=\"idea.nombre\"></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label position=\"stacked\"><h1>Color</h1></ion-label>\n    <ion-select [(ngModel)]=\"idea.color\">\n      <ion-select-option value=\"negro\">Negra</ion-select-option>\n      <ion-select-option value=\"blanca\">Blanca</ion-select-option>\n      <ion-select-option value=\"amarilla\">Amarilla</ion-select-option>\n      <ion-select-option value=\"gris\">Gris</ion-select-option>\n      <ion-select-option value=\"rosa\">Rosa</ion-select-option>\n      <ion-select-option value=\"azul\">Azul</ion-select-option>\n    </ion-select>\n  </ion-item>\n  <ion-item>\n    <ion-label position=\"stacked\"><h1>Tallas disponibles</h1></ion-label>\n    <ion-select multiple=\"true\" [(ngModel)]=\"idea.talla\">\n      <ion-select-option value=\"XS\">XS</ion-select-option>\n      <ion-select-option value=\"S\">S</ion-select-option>\n      <ion-select-option value=\"M\">M</ion-select-option>\n      <ion-select-option value=\"L\">L</ion-select-option>\n      <ion-select-option value=\"XL\">XL</ion-select-option>\n      <ion-select-option value=\"XXL\">XXL</ion-select-option>\n    </ion-select>\n  </ion-item>\n  <ion-item>\n    <ion-label position=\"stacked\"><h1>Genero</h1></ion-label>\n    <ion-select [(ngModel)]=\"idea.genero\">\n      <ion-select-option value=\"chica\">Chica</ion-select-option>\n      <ion-select-option value=\"chico\">Chico</ion-select-option>\n      <ion-select-option value=\"unisex\">Unisex</ion-select-option>\n    </ion-select>\n  </ion-item>\n  <ion-item>\n    <ion-label position=\"stacked\"><h1>Stock</h1></ion-label>\n    <ion-input [(ngModel)]=\"idea.stock\"></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label position=\"stacked\"><h1>Precio</h1></ion-label>\n    <ion-input [(ngModel)]=\"idea.precio\"></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label position=\"stacked\"><h1>Imagen 1</h1></ion-label>\n    <ion-input [(ngModel)]=\"idea.imagen0\"></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label position=\"stacked\"><h1>Imagen 2</h1></ion-label>\n    <ion-input [(ngModel)]=\"idea.imagen1\"></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label position=\"stacked\"><h1>Imagen 3</h1></ion-label>\n    <ion-input [(ngModel)]=\"idea.imagen2\"></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label position=\"stacked\"><h1>Categoria</h1></ion-label>\n    <ion-select [(ngModel)]=\"idea.categoria\">\n      <ion-select-option value=\"peliculas\">Peliculas</ion-select-option>\n      <ion-select-option value=\"bandas\">Bandas</ion-select-option>\n      <ion-select-option value=\"series\">Series</ion-select-option>\n    </ion-select>\n  </ion-item>\n  <ion-item>\n    <ion-label position=\"stacked\"><h1>Tipo</h1></ion-label>\n    <ion-select [(ngModel)]=\"idea.tipo\">\n      <ion-select-option value=\"camiseta\">Camiseta</ion-select-option>\n      <ion-select-option value=\"accesorios\">Accesorios</ion-select-option>\n    </ion-select>\n  </ion-item>\n</ion-content>\n\n<ion-footer *ngIf=\"!idea.id\">\n  <ion-toolbar color=\"dark\">\n    <ion-button expand=\"full\" fill=\"clear\" color=\"light\" (click)=\"addIdea()\">\n      Agregar Producto\n    </ion-button>\n  </ion-toolbar>\n</ion-footer>\n\n<ion-footer *ngIf=\"idea.id\">\n  <ion-row style=\"margin-bottom: 3%;\" class=\"ion-text-center\">\n    <ion-col size=\"6\">\n      <ion-button\n        style=\"height: 200%;\"\n        expand=\"block\"\n        fill=\"outline\"\n        color=\"danger\"\n        (click)=\"presentAlertConfirm()\"\n      >\n        <ion-icon name=\"trash\" slot=\"start\"></ion-icon>\n        Eliminar\n      </ion-button>\n    </ion-col>\n    <ion-col size=\"6\">\n      <ion-button\n        style=\"height: 200%;\"\n        expand=\"block\"\n        fill=\"solid\"\n        color=\"primary\"\n        (click)=\"updateIdea()\"\n      >\n        <ion-icon name=\"save\" slot=\"start\"></ion-icon>\n        Actualizar\n      </ion-button>\n    </ion-col>\n  </ion-row>\n</ion-footer>\n"

/***/ }),

/***/ "./src/app/pages/idea-details/idea-details.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/idea-details/idea-details.module.ts ***!
  \***********************************************************/
/*! exports provided: IdeaDetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IdeaDetailsPageModule", function() { return IdeaDetailsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _idea_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./idea-details.page */ "./src/app/pages/idea-details/idea-details.page.ts");







const routes = [
    {
        path: '',
        component: _idea_details_page__WEBPACK_IMPORTED_MODULE_6__["IdeaDetailsPage"]
    }
];
let IdeaDetailsPageModule = class IdeaDetailsPageModule {
};
IdeaDetailsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_idea_details_page__WEBPACK_IMPORTED_MODULE_6__["IdeaDetailsPage"]]
    })
], IdeaDetailsPageModule);



/***/ }),

/***/ "./src/app/pages/idea-details/idea-details.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/idea-details/idea-details.page.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2lkZWEtZGV0YWlscy9pZGVhLWRldGFpbHMucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/idea-details/idea-details.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/idea-details/idea-details.page.ts ***!
  \*********************************************************/
/*! exports provided: IdeaDetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IdeaDetailsPage", function() { return IdeaDetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_services_idea_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/idea.service */ "./src/app/services/idea.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");





let IdeaDetailsPage = class IdeaDetailsPage {
    constructor(activatedRoute, ideaService, toastCtrl, router, alertController) {
        this.activatedRoute = activatedRoute;
        this.ideaService = ideaService;
        this.toastCtrl = toastCtrl;
        this.router = router;
        this.alertController = alertController;
        this.idea = {
            nombre: "",
            color: "",
            talla: [""],
            genero: "",
            stock: "",
            precio: "",
            imagen0: "",
            imagen1: "",
            imagen2: "",
            categoria: "",
            tipo: ""
        };
    }
    ngOnInit() {
        let id = this.activatedRoute.snapshot.paramMap.get("id");
        if (id) {
            this.ideaService.getIdea(id).subscribe(idea => {
                this.idea = idea;
            });
        }
    }
    ionViewWillEnter() {
        let id = this.activatedRoute.snapshot.paramMap.get("id");
        if (id) {
            this.ideaService.getIdea(id).subscribe(idea => {
                this.idea = idea;
            });
        }
    }
    addIdea() {
        this.ideaService.addIdea(this.idea).then(() => {
            this.router.navigateByUrl("/");
            this.showToast("Producto agregado.");
        }, err => {
            this.showToast("Hubo un problema al agregar el producto.");
        });
    }
    deleteIdea() {
        this.ideaService.deleteIdea(this.idea.id).then(() => {
            this.router.navigateByUrl("/");
            this.showToast("Producto eliminado.");
        }, err => {
            this.showToast("No se pudo eliminar el producto.");
        });
    }
    updateIdea() {
        this.ideaService.updateIdea(this.idea).then(() => {
            this.router.navigateByUrl("/");
            this.showToast("Producto Actualizado");
        }, err => {
            this.showToast("Hubo un problema al actualizar el producto.");
        });
    }
    showToast(msg) {
        this.toastCtrl
            .create({
            message: msg,
            duration: 1500,
            color: "dark"
        })
            .then(toast => toast.present());
    }
    presentAlertConfirm() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: "¿Está seguro de eliminar?",
                message: "Presione Aceptar para eliminar o cancelar para volver.",
                buttons: [
                    {
                        text: "Cancelar",
                        role: "cancel",
                        cssClass: "secondary",
                        handler: blah => {
                            console.log("Confirm Cancel: c:");
                        }
                    },
                    {
                        text: "Aceptar",
                        handler: () => {
                            console.log("Confirm Okay");
                            this.deleteIdea();
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
};
IdeaDetailsPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: src_app_services_idea_service__WEBPACK_IMPORTED_MODULE_3__["IdeaService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] }
];
IdeaDetailsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-idea-details",
        template: __webpack_require__(/*! raw-loader!./idea-details.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/idea-details/idea-details.page.html"),
        styles: [__webpack_require__(/*! ./idea-details.page.scss */ "./src/app/pages/idea-details/idea-details.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
        src_app_services_idea_service__WEBPACK_IMPORTED_MODULE_3__["IdeaService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]])
], IdeaDetailsPage);



/***/ })

}]);
//# sourceMappingURL=pages-idea-details-idea-details-module-es2015.js.map