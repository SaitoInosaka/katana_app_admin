(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-view-product-view-product-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/view-product/view-product.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/view-product/view-product.page.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"dark\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/\"></ion-back-button>\n    </ion-buttons>\n    <ion-title style=\"text-align: center;\">{{ idea.nombre }}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-item></ion-item>\n  <ion-item>\n    <ion-label> <b>Color:</b> {{ idea.color }} </ion-label>\n    <ion-label> <b>Tallas:</b> {{ idea.talla }} </ion-label>\n  </ion-item>\n  <ion-item>\n    <ion-label> <b>Stock:</b> {{ idea.stock }} </ion-label>\n    <ion-label> <b>Precio($):</b> {{ idea.precio }} </ion-label>\n  </ion-item>\n  <ion-item>\n    <ion-label> <b>Categoria:</b> {{ idea.categoria }} </ion-label>\n    <ion-label> <b>Tipo:</b> {{ idea.tipo }} </ion-label>\n  </ion-item>\n  <ion-item>\n    <ion-label> <b>Genero:</b> {{ idea.genero }} </ion-label>\n  </ion-item>\n  <ion-item></ion-item>\n  <ion-item>\n    <ion-img\n      [src]=\"idea.imagen0\"\n      style=\"width: 20%; text-align: center; margin-left: 10%;\"\n    ></ion-img>\n    <ion-img\n      [src]=\"idea.imagen1\"\n      style=\"width: 20%; text-align: center; margin-left: 5%;\"\n    ></ion-img>\n\n    <ion-img\n      [src]=\"idea.imagen2\"\n      style=\"width: 20%; text-align: center; margin-left: 5%;\"\n    ></ion-img>\n  </ion-item>\n\n  <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n    <ion-fab-button color=\"dark\" button [routerLink]=\"['/idea', idea.id]\">\n      <ion-icon name=\"create\"></ion-icon>\n    </ion-fab-button>\n  </ion-fab>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/view-product/view-product.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/view-product/view-product.module.ts ***!
  \***********************************************************/
/*! exports provided: ViewProductPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewProductPageModule", function() { return ViewProductPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _view_product_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./view-product.page */ "./src/app/pages/view-product/view-product.page.ts");







var routes = [
    {
        path: '',
        component: _view_product_page__WEBPACK_IMPORTED_MODULE_6__["ViewProductPage"]
    }
];
var ViewProductPageModule = /** @class */ (function () {
    function ViewProductPageModule() {
    }
    ViewProductPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_view_product_page__WEBPACK_IMPORTED_MODULE_6__["ViewProductPage"]]
        })
    ], ViewProductPageModule);
    return ViewProductPageModule;
}());



/***/ }),

/***/ "./src/app/pages/view-product/view-product.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/view-product/view-product.page.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3ZpZXctcHJvZHVjdC92aWV3LXByb2R1Y3QucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/view-product/view-product.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/view-product/view-product.page.ts ***!
  \*********************************************************/
/*! exports provided: ViewProductPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewProductPage", function() { return ViewProductPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_idea_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/idea.service */ "./src/app/services/idea.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");





var ViewProductPage = /** @class */ (function () {
    function ViewProductPage(activatedRoute, ideaService, toastCtrl, router, alertController) {
        this.activatedRoute = activatedRoute;
        this.ideaService = ideaService;
        this.toastCtrl = toastCtrl;
        this.router = router;
        this.alertController = alertController;
        this.idea = {
            nombre: "",
            color: "",
            talla: [""],
            genero: "",
            stock: "",
            precio: "",
            imagen0: "",
            imagen1: "",
            imagen2: "",
            categoria: "",
            tipo: ""
        };
    }
    ViewProductPage.prototype.ngOnInit = function () {
        var _this = this;
        var id = this.activatedRoute.snapshot.paramMap.get("id");
        if (id) {
            this.ideaService.getIdea(id).subscribe(function (idea) {
                _this.idea = idea;
            });
        }
    };
    ViewProductPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: src_app_services_idea_service__WEBPACK_IMPORTED_MODULE_3__["IdeaService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] }
    ]; };
    ViewProductPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-view-product",
            template: __webpack_require__(/*! raw-loader!./view-product.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/view-product/view-product.page.html"),
            styles: [__webpack_require__(/*! ./view-product.page.scss */ "./src/app/pages/view-product/view-product.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            src_app_services_idea_service__WEBPACK_IMPORTED_MODULE_3__["IdeaService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]])
    ], ViewProductPage);
    return ViewProductPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-view-product-view-product-module-es5.js.map