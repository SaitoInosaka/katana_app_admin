(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-idea-details-idea-details-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/idea-details/idea-details.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/idea-details/idea-details.page.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>ideaDetails</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-paddign\">\n  <ion-item>\n    <ion-label position=\"stacked\">Nombre</ion-label>\n    <ion-input [(ngModel)]=\"idea.nombre\"></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label position=\"stacked\">Color</ion-label>\n    <ion-select [(ngModel)]=\"idea.color\">\n      <ion-select-option value=\"negro\">Negra</ion-select-option>\n      <ion-select-option value=\"blanca\">Blanca</ion-select-option>\n      <ion-select-option value=\"amarilla\">Amarilla</ion-select-option>\n      <ion-select-option value=\"gris\">Gris</ion-select-option>\n      <ion-select-option value=\"rosa\">Rosa</ion-select-option>\n      <ion-select-option value=\"azuk\">Azul</ion-select-option>\n    </ion-select>\n  </ion-item>\n  <ion-item>\n    <ion-label position=\"stacked\">Tallas disponibles</ion-label>\n    <ion-select multiple=\"true\" [(ngModel)]=\"idea.talla\">\n      <ion-select-option value=\"XS\">XS</ion-select-option>\n      <ion-select-option value=\"S\">S</ion-select-option>\n      <ion-select-option value=\"M\">M</ion-select-option>\n      <ion-select-option value=\"L\">L</ion-select-option>\n      <ion-select-option value=\"XL\">XL</ion-select-option>\n      <ion-select-option value=\"XXL\">XXL</ion-select-option>\n    </ion-select>\n  </ion-item>\n  <ion-item>\n    <ion-label position=\"stacked\">Stock</ion-label>\n    <ion-input [(ngModel)]=\"idea.stock\"></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label position=\"stacked\">Precio</ion-label>\n    <ion-input [(ngModel)]=\"idea.precio\"></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label position=\"stacked\">Imagen 1</ion-label>\n    <ion-input [(ngModel)]=\"idea.imagen0\"></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label position=\"stacked\">Imagen 2</ion-label>\n    <ion-input [(ngModel)]=\"idea.imagen1\"></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label position=\"stacked\">Imagen 3</ion-label>\n    <ion-input [(ngModel)]=\"idea.imagen2\"></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label position=\"stacked\">Categoria</ion-label>\n    <ion-select [(ngModel)]=\"idea.categoria\">\n      <ion-select-option value=\"peliculas\">Peliculas</ion-select-option>\n      <ion-select-option value=\"bandas\">Bandas</ion-select-option>\n      <ion-select-option value=\"series\">Series</ion-select-option>\n    </ion-select>\n  </ion-item>\n  <ion-item>\n    <ion-label position=\"stacked\">Tipo</ion-label>\n    <ion-select [(ngModel)]=\"idea.tipo\">\n      <ion-select-option value=\"camiseta\">Camiseta</ion-select-option>\n      <ion-select-option value=\"accesorios\">Accesorios</ion-select-option>\n    </ion-select>\n  </ion-item>\n</ion-content>\n\n<ion-footer *ngIf=\"!idea.id\">\n  <ion-toolbar color=\"success\">\n    <ion-button expand=\"full\" fill=\"clear\" color=\"light\" (click)=\"addIdea()\">\n      <ion-icon name=\"checkmark\" slot=\"start\">\n        Agregar Producto\n      </ion-icon>\n    </ion-button>\n  </ion-toolbar>\n</ion-footer>\n\n<ion-footer *ngIf=\"idea.id\">\n  <ion-row class=\"ion-no-padding ion-text-center\">\n    <ion-col size=\"6\">\n      <ion-button\n        expand=\"block\"\n        fill=\"outline\"\n        color=\"danger\"\n        (click)=\"presentAlertConfirm()\"\n      >\n        <ion-icon name=\"trash\" slot=\"start\"></ion-icon>\n        Delete\n      </ion-button>\n    </ion-col>\n    <ion-col size=\"6\">\n      <ion-button\n        expand=\"block\"\n        fill=\"solid\"\n        color=\"primary\"\n        (click)=\"updateIdea()\"\n      >\n        <ion-icon name=\"save\" slot=\"start\"></ion-icon>\n        Update\n      </ion-button>\n    </ion-col>\n  </ion-row>\n</ion-footer>\n"

/***/ }),

/***/ "./src/app/pages/idea-details/idea-details.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/idea-details/idea-details.module.ts ***!
  \***********************************************************/
/*! exports provided: IdeaDetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IdeaDetailsPageModule", function() { return IdeaDetailsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _idea_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./idea-details.page */ "./src/app/pages/idea-details/idea-details.page.ts");







var routes = [
    {
        path: '',
        component: _idea_details_page__WEBPACK_IMPORTED_MODULE_6__["IdeaDetailsPage"]
    }
];
var IdeaDetailsPageModule = /** @class */ (function () {
    function IdeaDetailsPageModule() {
    }
    IdeaDetailsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_idea_details_page__WEBPACK_IMPORTED_MODULE_6__["IdeaDetailsPage"]]
        })
    ], IdeaDetailsPageModule);
    return IdeaDetailsPageModule;
}());



/***/ }),

/***/ "./src/app/pages/idea-details/idea-details.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/idea-details/idea-details.page.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2lkZWEtZGV0YWlscy9pZGVhLWRldGFpbHMucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/idea-details/idea-details.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/idea-details/idea-details.page.ts ***!
  \*********************************************************/
/*! exports provided: IdeaDetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IdeaDetailsPage", function() { return IdeaDetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_idea_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/idea.service */ "./src/app/services/idea.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");





var IdeaDetailsPage = /** @class */ (function () {
    function IdeaDetailsPage(activatedRoute, ideaService, toastCtrl, router, alertController) {
        this.activatedRoute = activatedRoute;
        this.ideaService = ideaService;
        this.toastCtrl = toastCtrl;
        this.router = router;
        this.alertController = alertController;
        this.idea = {
            nombre: "",
            color: "",
            talla: [""],
            stock: "",
            precio: "",
            imagen0: "",
            imagen1: "",
            imagen2: "",
            categoria: "",
            tipo: ""
        };
    }
    IdeaDetailsPage.prototype.ngOnInit = function () {
        var _this = this;
        var id = this.activatedRoute.snapshot.paramMap.get("id");
        if (id) {
            this.ideaService.getIdea(id).subscribe(function (idea) {
                _this.idea = idea;
            });
        }
    };
    IdeaDetailsPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        var id = this.activatedRoute.snapshot.paramMap.get("id");
        if (id) {
            this.ideaService.getIdea(id).subscribe(function (idea) {
                _this.idea = idea;
            });
        }
    };
    IdeaDetailsPage.prototype.addIdea = function () {
        var _this = this;
        this.ideaService.addIdea(this.idea).then(function () {
            _this.router.navigateByUrl("/");
            _this.showToast("Producto agregado.");
        }, function (err) {
            _this.showToast("Hubo un problema al agregar el producto.");
        });
    };
    IdeaDetailsPage.prototype.deleteIdea = function () {
        var _this = this;
        this.ideaService.deleteIdea(this.idea.id).then(function () {
            _this.router.navigateByUrl("/");
            _this.showToast("Producto eliminado.");
        }, function (err) {
            _this.showToast("No se pudo eliminar el producto.");
        });
    };
    IdeaDetailsPage.prototype.updateIdea = function () {
        var _this = this;
        this.ideaService.updateIdea(this.idea).then(function () {
            _this.router.navigateByUrl("/");
            _this.showToast("Producto Actualizado");
        }, function (err) {
            _this.showToast("Hubo un problema al actualizar el producto.");
        });
    };
    IdeaDetailsPage.prototype.showToast = function (msg) {
        this.toastCtrl
            .create({
            message: msg,
            duration: 1500,
            color: "dark"
        })
            .then(function (toast) { return toast.present(); });
    };
    IdeaDetailsPage.prototype.presentAlertConfirm = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: "¿Está seguro de eliminar?",
                            message: "Presione Aceptar para eliminar o cancelar para volver.",
                            buttons: [
                                {
                                    text: "Cancelar",
                                    role: "cancel",
                                    cssClass: "secondary",
                                    handler: function (blah) {
                                        console.log("Confirm Cancel: c:");
                                    }
                                },
                                {
                                    text: "Aceptar",
                                    handler: function () {
                                        console.log("Confirm Okay");
                                        _this.deleteIdea();
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    IdeaDetailsPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: src_app_services_idea_service__WEBPACK_IMPORTED_MODULE_3__["IdeaService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] }
    ]; };
    IdeaDetailsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-idea-details",
            template: __webpack_require__(/*! raw-loader!./idea-details.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/idea-details/idea-details.page.html"),
            styles: [__webpack_require__(/*! ./idea-details.page.scss */ "./src/app/pages/idea-details/idea-details.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            src_app_services_idea_service__WEBPACK_IMPORTED_MODULE_3__["IdeaService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]])
    ], IdeaDetailsPage);
    return IdeaDetailsPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-idea-details-idea-details-module-es5.js.map