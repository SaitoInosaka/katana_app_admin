import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthGuard } from "./guards/auth.guard";
const routes: Routes = [
  {
    path: "",
    loadChildren: "./pages/idea-list/idea-list.module#IdeaListPageModule",
    canActivate: [AuthGuard]
  },
  {
    path: "idea",
    loadChildren:
      "./pages/idea-details/idea-details.module#IdeaDetailsPageModule",
    canActivate: [AuthGuard]
  },
  {
    path: "idea/:id",
    loadChildren:
      "./pages/idea-details/idea-details.module#IdeaDetailsPageModule",
    canActivate: [AuthGuard]
  },
  {
    path: "products/:id",
    loadChildren:
      "./pages/view-product/view-product.module#ViewProductPageModule",
    canActivate: [AuthGuard]
  },
  { path: "login", loadChildren: "./pages/login/login.module#LoginPageModule" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
