import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { Idea, IdeaService } from "src/app/services/idea.service";
import { LoadingController } from "@ionic/angular";
import { Router } from "@angular/router";
import { AngularFireAuth } from "@angular/fire/auth";

@Component({
  selector: "app-idea-list",
  templateUrl: "./idea-list.page.html",
  styleUrls: ["./idea-list.page.scss"]
})
export class IdeaListPage implements OnInit {
  public ideas: Observable<Idea[]>; //cambie de private a public porque no me dejaba ejecutar 'ionic build --prod --release'

  constructor(
    private ideaService: IdeaService,
    private loadingController: LoadingController,
    private authSvc: IdeaService,
    private router: Router,
    private afAuth: AngularFireAuth
  ) {}

  ngOnInit() {
    this.presentLoading();
    this.ideas = this.ideaService.getIdeas();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      duration: 1500
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

    console.log("Loading dismissed!");
  }

  onLogout() {
    console.log("Logout!");
    this.afAuth.auth.signOut();
    this.router.navigateByUrl("/login");
  }
}
