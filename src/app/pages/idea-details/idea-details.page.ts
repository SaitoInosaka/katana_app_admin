import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { IdeaService, Idea } from "src/app/services/idea.service";
import { AlertController, ToastController } from "@ionic/angular";

@Component({
  selector: "app-idea-details",
  templateUrl: "./idea-details.page.html",
  styleUrls: ["./idea-details.page.scss"]
})
export class IdeaDetailsPage implements OnInit {
  idea: Idea = {
    nombre: "",
    color: "",
    talla: [""],
    genero: "",
    stock: "",
    precio: "",
    imagen0: "",
    imagen1: "",
    imagen2: "",
    categoria: "",
    tipo: ""
  };
  constructor(
    private activatedRoute: ActivatedRoute,
    private ideaService: IdeaService,
    private toastCtrl: ToastController,
    private router: Router,
    private alertController: AlertController
  ) {}

  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get("id");
    if (id) {
      this.ideaService.getIdea(id).subscribe(idea => {
        this.idea = idea;
      });
    }
  }

  ionViewWillEnter() {
    let id = this.activatedRoute.snapshot.paramMap.get("id");
    if (id) {
      this.ideaService.getIdea(id).subscribe(idea => {
        this.idea = idea;
      });
    }
  }

  addIdea() {
    this.ideaService.addIdea(this.idea).then(
      () => {
        this.router.navigateByUrl("/");
        this.showToast("Producto agregado.");
      },
      err => {
        this.showToast("Hubo un problema al agregar el producto.");
      }
    );
  }

  deleteIdea() {
    this.ideaService.deleteIdea(this.idea.id).then(
      () => {
        this.router.navigateByUrl("/");
        this.showToast("Producto eliminado.");
      },
      err => {
        this.showToast("No se pudo eliminar el producto.");
      }
    );
  }

  updateIdea() {
    this.ideaService.updateIdea(this.idea).then(
      () => {
        this.router.navigateByUrl("/");
        this.showToast("Producto Actualizado");
      },
      err => {
        this.showToast("Hubo un problema al actualizar el producto.");
      }
    );
  }

  showToast(msg) {
    this.toastCtrl
      .create({
        message: msg,
        duration: 1500,
        color: "dark"
      })
      .then(toast => toast.present());
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: "¿Está seguro de eliminar?",
      message: "Presione Aceptar para eliminar o cancelar para volver.",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          cssClass: "secondary",
          handler: blah => {
            console.log("Confirm Cancel: c:");
          }
        },
        {
          text: "Aceptar",
          handler: () => {
            console.log("Confirm Okay");
            this.deleteIdea();
          }
        }
      ]
    });

    await alert.present();
  }
}
