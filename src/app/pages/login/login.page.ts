import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Idea, IdeaService } from "src/app/services/idea.service";
import { User } from "../../shared/user.class";
import { AlertController } from "@ionic/angular";

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"]
})
export class LoginPage implements OnInit {
  user: User = new User();
  constructor(
    private router: Router,
    private authSvc: IdeaService,
    private alertController: AlertController
  ) {}

  ngOnInit() {}

  async onLogin() {
    const user = await this.authSvc.onLogin(this.user);
    if (user) {
      console.log("Loggin success.");
      this.router.navigateByUrl("/");
    } else {
      this.presentAlert("Usuario y/o contra incorrectas.");
    }
  }

  async presentAlert(msg) {
    const alert = await this.alertController.create({
      header: "D:",
      message: msg,
      buttons: ["OK"]
    });

    await alert.present();
  }
}
