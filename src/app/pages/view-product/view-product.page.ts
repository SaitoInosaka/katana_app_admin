import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { IdeaService, Idea } from "src/app/services/idea.service";
import { AlertController, ToastController } from "@ionic/angular";

@Component({
  selector: "app-view-product",
  templateUrl: "./view-product.page.html",
  styleUrls: ["./view-product.page.scss"]
})
export class ViewProductPage implements OnInit {
  idea: Idea = {
    nombre: "",
    color: "",
    talla: [""],
    genero: "",
    stock: "",
    precio: "",
    imagen0: "",
    imagen1: "",
    imagen2: "",
    categoria: "",
    tipo: ""
  };
  constructor(
    private activatedRoute: ActivatedRoute,
    private ideaService: IdeaService,
    private toastCtrl: ToastController,
    private router: Router,
    private alertController: AlertController
  ) {}

  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get("id");
    if (id) {
      this.ideaService.getIdea(id).subscribe(idea => {
        this.idea = idea;
      });
    }
  }
}
