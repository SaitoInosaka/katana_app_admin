import { Injectable } from "@angular/core";
import {
  AngularFirestore,
  AngularFirestoreCollection,
  DocumentReference
} from "@angular/fire/firestore";
import { map, take } from "rxjs/operators";
import { Observable } from "rxjs";

import { User } from "../shared/user.class";
import { AngularFireAuth } from "@angular/fire/auth";

export interface Idea {
  id?: string;
  nombre: string; //nombre del producto
  color: string; //color del producto
  talla: string[]; //talla disponible por ahora solo 1
  genero: string;
  stock: string; //cantidad del producto
  precio: string;
  imagen0: string;
  imagen1: string;
  imagen2: string;
  categoria: string; //si es peliculas, series o bandas
  tipo: string; //tipo de producto si es camiseta o accesorios
}

@Injectable({
  providedIn: "root"
})
export class IdeaService {
  user: User = new User();
  public isLogged: any = false;
  private ideas: Observable<Idea[]>;
  private ideaCollection: AngularFirestoreCollection<Idea>;

  constructor(private afs: AngularFirestore, public afAuth: AngularFireAuth) {
    afAuth.authState.subscribe(user => (this.isLogged = user));
    this.ideaCollection = this.afs.collection<Idea>("ideas");
    this.ideas = this.ideaCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  getIdeas(): Observable<Idea[]> {
    return this.ideas;
  }

  getIdea(id: string): Observable<Idea> {
    return this.ideaCollection
      .doc<Idea>(id)
      .valueChanges()
      .pipe(
        take(1),
        map(idea => {
          idea.id = id;
          return idea;
        })
      );
  }

  addIdea(idea: Idea): Promise<DocumentReference> {
    return this.ideaCollection.add(idea);
  }

  updateIdea(idea: Idea): Promise<void> {
    return this.ideaCollection.doc(idea.id).update({
      nombre: idea.nombre,
      color: idea.color,
      talla: idea.talla,
      genero: idea.genero,
      stock: idea.stock,
      precio: idea.precio,
      imagen0: idea.imagen0,
      imagen1: idea.imagen1,
      imagen2: idea.imagen2,
      categoria: idea.categoria,
      tipo: idea.tipo
    });
  }

  deleteIdea(id: string): Promise<void> {
    return this.ideaCollection.doc(id).delete();
  }

  async onLogin(user: User) {
    try {
      return await this.afAuth.auth.signInWithEmailAndPassword(
        user.email,
        user.password
      );
    } catch (error) {
      console.log("Error on login", error);
    }
  }
}
