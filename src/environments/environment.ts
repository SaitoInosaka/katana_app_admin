// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDXzRx8xOndLutJVGJ7Iy0QP3UX-E4RzRg",
    authDomain: "katana-app-admin-e0365.firebaseapp.com",
    databaseURL: "https://katana-app-admin-e0365.firebaseio.com",
    projectId: "katana-app-admin-e0365",
    storageBucket: "katana-app-admin-e0365.appspot.com",
    messagingSenderId: "310690061779",
    appId: "1:310690061779:web:4c05a7c2cd10c0d0d52326",
    measurementId: "G-X1J1F3LJGH"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
